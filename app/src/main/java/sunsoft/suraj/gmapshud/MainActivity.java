package sunsoft.suraj.gmapshud;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import java.util.Set;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.TextView;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;
import java.io.IOException;
import java.util.UUID;
import android.bluetooth.BluetoothSocket;

public class MainActivity extends Activity {

    Button btnPaired;

    TextView txt;

    private NotificationReceiver nReceiver;

    private BluetoothAdapter myBluetooth = null;

    String address = null;
    String btname = null;

    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    ConnectBT _bt = new ConnectBT();

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(btnPaired, "Press BACK again to exit", Snackbar.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_main);

        btnPaired = findViewById(R.id.button3);
        txt = findViewById(R.id.notificationtext);

        /*Snackbar.make(btnPaired, "Your message here", Snackbar.LENGTH_SHORT).setAction("YourAction", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Your Action
            }
        }).show();

        Snackbar.make(btnPaired, "Your message here", Snackbar.LENGTH_SHORT).show();

        */




        nReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");
        registerReceiver(nReceiver,filter);

        if (!Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners").contains(getApplicationContext().getPackageName())) {
            Snackbar.make(btnPaired, "Please enable notification access", Snackbar.LENGTH_INDEFINITE).setAction("Enable", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Open Notification access settings
                    Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    startActivity(intent);
                }
            }).show();

        }

        btnPaired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                pairedDevicesList(); //method that will be called
            }
        });


        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null)
        {
            Snackbar.make(btnPaired, "Bluetooth Device Not Available", Snackbar.LENGTH_SHORT).show();
            finish();
        }
        else
        {
            if (!myBluetooth.isEnabled()) {
                //Ask to the user turn the bluetooth on
                Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnBTon,1);
            }
        }
    }

    @Override
    protected void onDestroy() {
        _bt.cancel(true);
        btSocket = null;
        isBtConnected = false;
        _bt = null;
        super.onDestroy();
        unregisterReceiver(nReceiver);
    }



    private void pairedDevicesList(){
        final AlertDialog.Builder setup = new AlertDialog.Builder(MainActivity.this);
        ListView list = new ListView(MainActivity.this);
        Set<BluetoothDevice> pairedDevices = myBluetooth.getBondedDevices();
        ArrayList listitems = new ArrayList();


        if (pairedDevices.size()>0)
        {
            for(BluetoothDevice bt: pairedDevices)
            {
                //String _name = bt.getName();
                //if (_name.equals("GmapsHUD")) {
                listitems.add(bt.getName() + "\n" + bt.getAddress());
                //}
            }
        }
        else
        {
            Snackbar.make(btnPaired, "No Paired Bluetooth Devices Found.", Snackbar.LENGTH_SHORT).show();
        }


        list.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, listitems));
        setup.setView(list);
        final AlertDialog alertview = setup.create();
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String info = ((TextView) view).getText().toString();
                address = info.substring(info.length() - 17);
                btname = info.substring(0,info.indexOf("\n"));

                //btSocket.close(); //close connection
                _bt.cancel(true);
                btSocket = null;
                isBtConnected = false;
                _bt = null;
                _bt = new ConnectBT();

                _bt.execute();
                alertview.dismiss();
            }
        });

        alertview.show();
    }



    @SuppressLint("StaticFieldLeak")
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            Snackbar.make(btnPaired, "Connecting to " + btname + "..." , Snackbar.LENGTH_INDEFINITE).setAction("Cancel", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Your Action
                    Snackbar.make(btnPaired, "Cancelled", Snackbar.LENGTH_SHORT).show();
                    ConnectSuccess = false;
                    _bt.cancel(true);
                    btSocket = null;
                    isBtConnected = false;
                    _bt = null;
                    _bt = new ConnectBT();
                    cancel(false);
                }
            }).show();
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                Snackbar.make(btnPaired, "Connection Failed. Is it a SPP Bluetooth? Try again.", Snackbar.LENGTH_LONG).show();
                _bt.cancel(true);
                btSocket = null;
                isBtConnected = false;
                _bt = null;
                _bt = new ConnectBT();
                //finish();
            }
            else
            {
                Snackbar.make(btnPaired, "Connected.", Snackbar.LENGTH_LONG).show();
                isBtConnected = true;
            }
            //progress.dismiss();
        }
    }

    class NotificationReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String temp = intent.getStringExtra("notification_event") + "\n";
            if (btSocket!=null && isBtConnected)
            {
                try {
                    btSocket.getOutputStream().write(temp.getBytes());
                    String tmp = getString(R.string.tobluetooth) + temp;
                    txt.setText(tmp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                String tmp = getString(R.string.toscreen) + temp;
                txt.setText(tmp);
            }
        }
    }
}


