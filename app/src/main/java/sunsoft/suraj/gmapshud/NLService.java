package sunsoft.suraj.gmapshud;

import android.app.Notification;
import android.content.BroadcastReceiver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.RemoteViews;


public class NLService extends NotificationListenerService {

    private final static boolean STORE_IMG = false;
    private final static String IMAGE_DIR = "/storage/emulated/0/Pictures/";

    private Arrow foundArrow = Arrow.None;

    private String TAG = this.getClass().getSimpleName();
    private NLServiceReceiver nlservicereciver;

    private String distanceNum = null;
    private String distanceUnit = null;
    private String remainHour = null;
    private String remainMinute = null;
    private String remainDistance = null;
    private String remainDistanceUnit = null;
    private String arrivalTime = null;


    @Override
    public void onCreate() {
        super.onCreate();
        nlservicereciver = new NLServiceReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("csunsoft.suraj.gmapshud.NOTIFICATION_LISTENER_SERVICE_EXAMPLE");
        registerReceiver(nlservicereciver,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nlservicereciver);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        //Log.i(TAG,"**********  onNotificationPosted");
        Log.i(TAG, sbn.getNotification().toString());
        Intent i = new  Intent("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");

        if (sbn.getPackageName().equals("com.google.android.apps.maps")) {
            i.putExtra("notification_event",processGoogleMapsNotification(sbn.getNotification()));
            sendBroadcast(i);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        //Log.i(TAG,"********** onNOtificationRemoved");
        //Log.i(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText +"\t" + sbn.getPackageName());
        Intent i = new  Intent("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");

        if (sbn.getPackageName().equals("com.google.android.apps.maps")) {
            i.putExtra("notification_event","-1" );

        }
        sendBroadcast(i);
    }

    private void storeBitmap(Bitmap bmp, String filename) throws IOException {
        FileOutputStream out = new FileOutputStream(filename);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
    }

    private String getArrow(ArrowImage image) {
        Arrow foundArrow = Arrow.None;
        StringBuilder tmp = new StringBuilder();
        for (Arrow a : Arrow.values()) {
            int sad = image.getSAD(a.value);
            tmp.append(Integer.toString(sad));
            /*if (0 == sad) {
                String shortString = Short.toString(a.value);
                Log.i(TAG, "Recognize " + a.name() + " " + shortString);
                foundArrow = a;
                break;
            }*/
        }
        Log.i(TAG, tmp.toString());
        return tmp.toString();
    }


    private static Bitmap removeAlpha(Bitmap originalBitmap) {
        // lets create a new empty bitmap
        Bitmap newBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        // create a canvas where we can draw on
        Canvas canvas = new Canvas(newBitmap);
        // create a paint instance with alpha
        Paint alphaPaint = new Paint();
        alphaPaint.setAlpha(255);
        // now lets draw using alphaPaint instance
        canvas.drawBitmap(originalBitmap, 0, 0, alphaPaint);
        return newBitmap;
    }


    private String processGoogleMapsNotification(Notification notification) {

        // We have to extract the information from the view
        RemoteViews views = notification.bigContentView;
        if (views == null) views = notification.contentView;
        if (views == null) return "-1";

        String arr = null;
        //long lastNotifyTimeMillis = System.currentTimeMillis();

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        try {
            Field fieldActions = views.getClass().getDeclaredField("mActions");
            fieldActions.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) fieldActions.get(views);

            int indexOfActions = 0;
            //int updateCount = 0;
            //boolean inNavigation = false;

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions) {
                Parcel parcel = Parcel.obtain();
                if (null == p) {
                    continue;
                }
                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                String simpleClassName = p.getClass().getSimpleName();
                if ( (tag != 2 && tag != 12) && (!simpleClassName.equals("ReflectionAction") && !simpleClassName.equals("BitmapReflectionAction")) )
                    continue;

                if(Build.VERSION.SDK_INT <28) {
                    // View ID
                    parcel.readInt();
                }

                String methodName = parcel.readString();

                if (methodName == null) continue;

                    // Save strings
                else if (methodName.equals("setText")) {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    String t = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel).toString().trim();
                    switch (indexOfActions) {
                        case 2:
//                            String exitNavigation = getString(R.string.exit_navigation);
                            //inNavigation = t.equalsIgnoreCase(getString(R.string.exit_navigation));
                            break;
                        case 3://distance to turn
                            parseDistanceToTurn(t);
                            break;
                        case 5://road
                            break;
                        case 8://time, distance, arrived time
                            parseTimeAndDistanceToDest(t);

                            //updateCount++;
                            break;
                    }
                } else if (methodName.equals("setImageBitmap")) {

                    int bitmapId = parcel.readInt();
                    Field fieldBitmapCache = views.getClass().getDeclaredField("mBitmapCache");
                    fieldBitmapCache.setAccessible(true);

                    Object bitmapCache = fieldBitmapCache.get(views);
                    Field fieldBitmaps = bitmapCache.getClass().getDeclaredField("mBitmaps");
                    fieldBitmaps.setAccessible(true);
                    Object bitmapsObject = fieldBitmaps.get(bitmapCache);

                    if (null != bitmapsObject) {
                        @SuppressWarnings("unchecked")
                        ArrayList<Bitmap> bitmapList = (ArrayList<Bitmap>) bitmapsObject;
                        Bitmap bitmapImage = bitmapList.get(bitmapId);
                        if (STORE_IMG) {
                            storeBitmap(bitmapImage, IMAGE_DIR + "arrow0.png");
                        }
                        bitmapImage = removeAlpha(bitmapImage);
                        if (STORE_IMG) {
                            storeBitmap(bitmapImage, IMAGE_DIR + "arrow.png");
                        }
                        ArrowImage arrowImage = new ArrowImage(bitmapImage);
                        //foundArrow = getArrow(arrowImage);

                        String tmp = getArrow(arrowImage);

                        switch (tmp){
                            case "455554535456736443364512": arr = "roundaboutLeft";
                                break;
                            case "411554575432550645546312": arr = "roundaboutRight";
                                break;
                            case "542423446743463532633013": arr = "roundaboutStraight";
                                break;
                            case "764201426765445532651213": arr = "Straight";
                                break;
                            case "564645062323445112255413": arr = "slightLeft";
                                break;
                            case "766023626587445754831413": arr = "MergeRight";
                                break;
                            case "546667260345665134257611": arr = "Left";
                                break;
                            case "302665664543661556457411": arr = "Right";
                                break;
                            case "653310537654534643562312": arr = "Goto";
                                break;
                            case "564443446345405534473613": arr = "ArrivalLeft";
                                break;
                            case "033776555432354467346512": arr = "SharpRight";
                                break;
                            case "675112517676336643740314": arr = "Arrival";
                                break;


                            default: arr = tmp;
                                break;
                        }
                        //updateCount++;
                    }

                }

                parcel.recycle();
                indexOfActions++;
            }

            return arr + "!" + distanceNum + distanceUnit + "!" + (null == remainHour ? 0 : remainHour) + ":" + remainMinute + "!" + remainDistance + remainDistanceUnit + "!" + arrivalTime;
        }

        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e) {
            Log.e("NotificationClassifier", e.toString());
            return "error";
        }

    }

    private void parseTimeAndDistanceToDest(String timeDistanceStirng) {
        String[] timeDistanceSplit = timeDistanceStirng.split("·");
//        remainTime = remainTimeUnit =
        remainHour = remainMinute = remainDistance = remainDistanceUnit = arrivalTime = null;

        if (3 == timeDistanceSplit.length) {
            String timeToDest = timeDistanceSplit[0].trim();
            String distanceToDest = timeDistanceSplit[1].trim();
            String timeToArrived = timeDistanceSplit[2].trim();

            //======================================================================================
            // remain time
            //======================================================================================
            String[] timeSplit = timeToDest.split(" ");
            if (4 == timeSplit.length) {
                remainHour = timeSplit[0].trim();
                remainMinute = timeSplit[2].trim();
                remainHour = remainHour.replaceAll("\u00A0", ""); // Remove spaces, .trim() seems not working
                remainMinute = remainMinute.replaceAll("\u00A0", "");
            } else if (2 == timeSplit.length) {
                final int hour_index = timeToDest.indexOf(getString(R.string.hour));
                final int minute_index = timeToDest.indexOf(getString(R.string.minute));
                if (-1 != hour_index && -1 != minute_index) {
                    remainHour = timeToDest.substring(0, hour_index).trim();
                    remainMinute = timeToDest.substring(hour_index + getString(R.string.hour).length(), minute_index).trim();
                    remainHour = remainHour.replaceAll("\u00A0", ""); // Remove spaces, .trim() seems not working
                    remainMinute = remainMinute.replaceAll("\u00A0", "");
                } else {
                    remainMinute = timeSplit[0].trim();
                    remainMinute = remainMinute.replaceAll("\u00A0", "");

                }

            } else if (1 == timeSplit.length) {
                timeSplit = splitDigitAndNonDigit(timeToDest);
                remainMinute = 2 == timeSplit.length ? timeSplit[0] : null;
            }
            //======================================================================================


            //======================================================================================
            // remain distance
            //======================================================================================
            String[] distSplit = distanceToDest.split(" ");
            if (2 != distSplit.length) {
                distSplit = splitDigitAndNonDigit(distanceToDest);
            }
            if (2 == distSplit.length) {
                remainDistance = distSplit[0].replaceAll("\u00A0", ""); // Remove spaces, .trim() doesn't work
                remainDistance = remainDistance.replace(",",".");
                remainDistanceUnit = distSplit[1].replaceAll("\u00A0", ""); // Remove spaces
                //remainDistanceUnit = translate(remainDistanceUnit);
            }

            //======================================================================================
            //ETA
            //======================================================================================
            final String ETA = getString(R.string.ETA);
            final int indexOfETA = timeToArrived.indexOf(ETA);
            String[] arrivedSplit;
            final boolean etaAtFirst = 0 == indexOfETA;
            if (etaAtFirst) {//前面, 應該是中文
                arrivedSplit = timeToArrived.split(ETA);
                arrivalTime = 2 == arrivedSplit.length ? arrivedSplit[1] : null;
            } else {//後面，可能是英文
                arrivedSplit = timeToArrived.split(ETA);
                arrivalTime = arrivedSplit[0];
            }

            arrivalTime = null != arrivalTime ? arrivalTime.trim() : null;
            assert arrivalTime != null;
            final int amIndex = arrivalTime.indexOf(getString(R.string.am));
            final int pmIndex = arrivalTime.indexOf(getString(R.string.pm));
            final boolean ampmAtFirst = 0 == amIndex || 0 == pmIndex;
            if (-1 != amIndex || -1 != pmIndex) {
                final int index = Math.max(amIndex, pmIndex);
                arrivalTime = ampmAtFirst ? arrivalTime.substring(index + 2) : arrivalTime.substring(0, index);
                arrivalTime = arrivalTime.trim();

                String[] split = arrivalTime.split(":");
                final int hh = Integer.parseInt(split[0]);
                if (-1 != pmIndex && 12 != hh) {
                    arrivalTime = (hh + 12) + ":" + split[1];
                }
            }
            //======================================================================================

        }

    }

    private String[] splitDigitAndAlphabetic(String str) {
        String[] result = new String[2];
        for (int x = 0; x < str.length(); x++) {
            char c = str.charAt(x);
            if (Character.isAlphabetic(c)) {
                result[0] = str.substring(0, x).replaceAll("\u00A0", ""); // Remove spaces, .trim() doesn't work
                result[1] = str.substring(x).replaceAll("\u00A0", ""); // Remove spaces
                break;
            }
        }
        return result;
    }

    private String[] splitDigitAndNonDigit(String str) {
        String[] result = new String[2];
        for (int x = 0; x < str.length(); x++) {
            char c = str.charAt(x);
            if (!Character.isDigit(c)) {
                result[0] = str.substring(0, x);
                result[1] = str.substring(x);
                break;
            }
        }
        return result;
    }

    private void parseDistanceToTurn(String distanceString) {


        if (!distanceString.isEmpty()) {
            final int indexOfHo = distanceString.indexOf("後");
            if (-1 != indexOfHo) {
                distanceString = distanceString.substring(0, indexOfHo);

            }
            String[] splitArray = distanceString.split(" ");
            if (2 != splitArray.length) {
                splitArray = splitDigitAndAlphabetic(distanceString);
            }
            String num = null;
            String unit = null;
            if (splitArray.length == 2) {
                num = splitArray[0].replace(",", ".");
                unit = splitArray[1];
            }

            distanceNum = num;
            distanceUnit = unit;
        }else{
            distanceNum = " ";
            distanceUnit = " ";
        }
    }




    class NLServiceReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("command").equals("clearall")){
                NLService.this.cancelAllNotifications();
            }
            else if(intent.getStringExtra("command").equals("list")){
                Intent i1 = new  Intent("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");
                i1.putExtra("notification_event","=====================");
                sendBroadcast(i1);
                int i=1;
                for (StatusBarNotification sbn : NLService.this.getActiveNotifications()) {
                    Intent i2 = new  Intent("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");
                    i2.putExtra("notification_event",i +" " + sbn.getPackageName() + "\n");
                    sendBroadcast(i2);
                    i++;
                }
                Intent i3 = new  Intent("sunsoft.suraj.gmapshud.NOTIFICATION_LISTENER");
                i3.putExtra("notification_event","===== Notification List ====");
                sendBroadcast(i3);

            }

        }
    }
}

class ArrowImage {

    private static final int IMAGE_LEN = 4;
    private static final int CONTENT_LEN = IMAGE_LEN * IMAGE_LEN;
    public boolean[] content = new boolean[CONTENT_LEN];

    ArrowImage(Bitmap bitmap) {

        final int interval = bitmap.getWidth() / IMAGE_LEN;
        for (int h0 = 0; h0 < IMAGE_LEN; h0++) {
            final int h = h0 * interval;
            for (int w0 = 0; w0 < IMAGE_LEN; w0++) {
                final int w = w0 * interval;
                int p = bitmap.getPixel(w, h);
                final int alpha = (p >> 24) & 0xff;
                final int max = Math.max(Math.max(p & 0xff, (p >> 8) & 0xff), (p >> 16) & 0xff);
                final int max_alpha = (max * alpha) >> 8;
                content[h0 * IMAGE_LEN + w0] = max_alpha < 254;
            }
        }

    }

    int getSAD(final int magicNumber) {
        int sad = 0;
        for (int x = 0; x < CONTENT_LEN; x++) {
            final boolean bit = 1 == ((magicNumber >> x) & 1);
            sad += content[x] != bit ? 1 : 0;
        }
        return sad;
    }

}
